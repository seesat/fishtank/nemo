# NEMO - DORY Coordination - 2024-05-07

## General discussion & updates

### NEMO Mission Design

- Achievements of past 2 weeks
    - weitere Orbitanalyse mit gmat
        -Orbithöhenvariation -> ca. 400km
        - Kontaktfrequenz und -dauer für bestimmten Bodenspot
        -EOL-Definition basierend auf Orbithöhe bei konservativen aerodynamischen Beiwerten
    - Betrachtung von anfallenden Datenmengen
        - AIS pro Schiff
        - Bilddaten
        - Telemetrie
    - nötige Auflösung für Kamera
        - 30kmx30km Bodenspot
        - Auflösung zu definieren, basiert auf Schiffgröße (aktuell Auflösung: PSD 5m)
- Planned for next 2 weeks
    - weitere Analyse der Kameraauflösung
    - weitere Orbitanalyse
    - Variation CubeSat-Größe
    - Power-Budget-Analyse
- Challenges/Blockers
    - Werte für Leistungsbedarf der Antenne benötigt
   	 
### DORY - Antenna
- Achievements of past 2 weeks
    - Matlab-Skripte
        - Link Budget erweitert, sodass Parametervariation leichter machbar
        - Beobachtungsbudget
    - maximale Anzahl Boote in einem geographischen Bereich
        - 4500 maximal (auf zwei Kanäle verteilt)
        - Größe des Spots hängt von Schiffdichte ab
    - Simulationsstrategie: Balun, Dipol, Helix, keine Monopol
        - Helix-Analyse durchgeführt
            - eher geringe Eignung, aufgrund Problemen durch begrenzten Durchmesser (D>200mm nötig) geringer
            - gute Anpassung, geringer Antennengewinn
        - Monopol
            - deutlich zu geringer Antennengewinn in LOS bei Nadir-Ausrichtung
            - Bei Verkippung/Ausrichtung parallel zu Nadir fehlt Groundplane; kein Vorteil gegenüber Dipol
        - Dipol
            - Simulationen laufend
            - Richtcharakteristik voraussichtlich in Ordnung (Keulenbreite)
            - Nutzung der zwei Dipolausleger zur Verdoppelung der Signalstärke
        - Balun: Anpassungsnetzwerk
            - erleichtert Erreichen von Unabhängigkeit von der Signalpolarisation
            - Vermeidung der Nutzung von Groundplane als Masse
            - aktuell idealisierte Annahme
        - Dipol aktuell Favorit, Helix eher mäßig geeignet
- Planned for next 2 weeks
    - Simulation
        - Dipolsimulation abschließen
        - voraussichtlich keine großen Änderungen
        - Vervollständigung der Dokumentation
    - Challenges/Blockers
        - mechanische Randbedingungen benötigt
   	 
### DORY - Mechanical
- Achievements of past 2 weeks
    - see DORY mission definition
    - keine Neuigkeiten für 
- Planned for next 2 weeks
    - weitere Bearbeitung von DORY Mission Definition
- Challenges/Blockers
    - NTR

## Discussions from Challenges/Blockers

### Werte für Leistungsbedarf der Antenne benötigt
- maximale Gesamtenergie pro Orbit
- einzige Last: AIS-Receiver, Antenne vollkommen passiv
- Vorgehen:
    - Signalleistung, die mit Antenne erreicht werden kann
    - AI-01 Pascal: Bestimmung ankommende Signalleistung (min. Wert)
    - Bestimmung der nötigen Receiver-Verstärkung basierend auf kommerziell verfügbaren Varianten
    - AI-02 Paul: Bestimmung der Verstärkerleistung basierend auf Signalstärke
### mechanische Randbedingungen benötigt
- von Interesse: mech. Begrenzungen und Anforderungen
    - maximale Dimensionen
    - Machbarkeit von Antennenformen
    - Materialauswahl
    - Dielektrizitätszahl
    - Permeabilität
    - Satellitenstruktur
- aktuell Größe undefiniert, Arbeitswert 8...12U
- Antennensimulation weiterhin mit 2U, um Vergleichbarkeit zu gewährleisten
- AI-03 Lena, Martin, Paul: Oben genannte Daten definieren

## Action Items
- AI-01 Pascal: Bestimmung ankommende Signalleistung (min. Wert)
- AI-02 Paul: Bestimmung der Verstärkerleistung basierend auf Signalstärke
- AI-03 Lena, Martin, Paul: Oben genannte Daten definieren

## DORY Mission Definition
- Discussions ongoing
    - AIS antenna vs more generic VHF antenna
    - current points of discussion
    - Mission Objectives
    - Mission Needs, Requirements, Constraints
    - Mission Concepts
- separate meeting to discuss more in-depth
