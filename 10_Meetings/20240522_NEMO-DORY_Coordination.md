# 2024-05-22 NEMO - DORY Coordination

## NEMO Mission Design

	- Achievements of past two weeks:
		- camera:
			- GSD: 3.xx m
				- Maksutov-Cassegrain
				- 2U for visible spectrum
				- larger (roughly 600 mm) for IR spectrum
			- Filter
				- necessary for visible spectrum
		- orbit:
			- continued comparison
			- min. mission duration: 0.5 years
		- communications:
			- comparison satellite - satellite vs. satellite - ground
			- UHF/VHF
			- latency  2h
			- communication mode strongly influences latency
		- Studienarbeit
			- new structure
			- improvement of poster
	
	- Planned work for next two weeks:
		- communications
			- continue analysis
			- define rough values for comparison
			- roughly calculate latency
		- camera
			- improve documentation
			- imaging: 5.8W for IR
			- idle: 2.5W for IR
		- power consumption
			- started rough total budget			
	
	- Challenges/Blockers:
		- missing values for power consumption

## DORY Antenna

	- Achievements of past two weeks:
		- Helix antenna
			- good directional antenna gain
			- very large
		- dipole antenna
			- smaller than Helix
			- good direction antenna gain
		- both types allow necessary antenna gain
		- antenna gain strongly affected by cubesat (changes in best length <20mm)
		- directional antenna gain pattern largely unaffected
		- LC loading
			- Matlab simulations started but not continued
			- very sensible for dipole Antenna
	
	- Planned work for next two weeks:
		- Documentation. A lot of documentation.
	
	- Challenges/Blockers:
		- none

## DORY Mechanical

	- Achievements of past two weeks:
		- created mission definition document in Valispace
		- started putting in information based on previous discussions

	
	- Planned work for next two weeks:
		- schedule mission objective discussion
		- begin discussing mechanical architectures for dipole antenna

	
	- Challenges/Blockers:
		- none


## Action Items
- Last week:
	- AI-01 Pascal: Bestimmung ankommende Signalleistung (min. Wert) - done
		- 20 fW
	- AI-02 Paul: Bestimmung der Verstärkerleistung basierend auf Signalstärke - done
		- zwischen 850mW und 1350 mW peak
		- zwischen 650mW und 850 mW average
	- AI-03 Lena, Martin, Paul: Mechanische Parameter definieren - done
- New:
	- AI-01 Luca: Abstimmung mit DORY-Antenne -> Interferenz von Sendesignalen und AIS-Signalen

## DORY Mission Definition
- Discussions ongoing
	- AIS antenna vs more generic VHF antenna
- current points of discussion
	- Mission Objectives
	- Mission Needs, Requirements, Constraints
	- Mission Concepts
- separate meeting to discuss more in-depth
