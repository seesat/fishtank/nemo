# NEMO-DORY Coordination 2024-06-05

## NEMO Mission Design

	- Achievements of past two weeks:
		- orbit simulation:
			- contact durations for 200km by 200km
			- number of contacts 
			- 10s as longest period between ship signals for moving ships
			- all signal that cannot be measured outside of this period gives false positive
			- roughly 10% false positives due to observation time of ground spot being too low
			- revisiting time of at minimum two days for single satellite
			- small optics lead to very small field of view at required GSD
		- optics:
			- last values still up-to-date
		- commanding / data rates
			- new use cases for data discussed
	
	- Planned work for next two weeks:
		- writing Studienarbeit
	
	- Challenges/Blockers:
		- larger ground spot area would be beneficial for improving observation time and reducing false positives

## DORY Antenna

	- Achievements of past two weeks:
		- finished first final draft of Studienarbeit
	
	- Planned work for next two weeks:
		- waiting for feedback
		- reduced workload due to exams
	
	- Challenges/Blockers:
		- none

## DORY Mechanical

	- Achievements of past two weeks:
		- consolidated requirements
		- started on DORY mission definition document
	
	- Planned work for next two weeks:
		- start on Mechanical concepts
	
	- Challenges/Blockers:
		- mission definition status not fully defined